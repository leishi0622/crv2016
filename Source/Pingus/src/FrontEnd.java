/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import beepbeep.*;
import marq.*;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Processor;
import ca.uqac.lif.cep.Pullable;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.Pullable.NextStatus;
import ca.uqac.lif.cep.Pushable;
import ca.uqac.lif.cep.epl.SinkLast;
import ca.uqac.lif.cep.functions.FunctionProcessor;
import ca.uqac.lif.cep.input.TokenFeeder;
import ca.uqac.lif.cep.io.LineReader;
import ca.uqac.lif.cep.ltl.Troolean;
import ca.uqac.lif.cep.sets.CsvToList;
import ca.uqac.lif.cep.xml.XmlReader;

/**
 * Front-end for running the CRV 2016 benchmarks with BeepBeep 3
 */
@SuppressWarnings("unused")
public class FrontEnd 
{
	public static void main(String[] args) throws FileNotFoundException, ConnectorException
	{
		System.out.println("BeepBeep 3 - A versatile event stream processor");
		System.out.println("(C) 2008-2016 Sylvain Hallé et al., Université du Québec à Chicoutimi");
		System.out.println("Front-end for CRV 2016");
		if (args.length < 2)
		{
			System.err.println("ERROR: incorrect number of arguments");
			System.exit(3);
		}
		String property_name = args[0];
		String filename = args[1];
		String num_pingus = "010";
		System.out.println("Evaluating benchmark " + property_name + " on " + filename);
		//filename = "/home/sylvain/Workspaces/pingu-generator/Source/Generator/all-ok-" + num_pingus + ".xml";
		//String property_type = "Pingus";
		String property_type = "MarQ";
		Processor prop = null;
		if (!new File(filename).exists())
		{
			System.err.println("ERROR: trace file not found");
			System.exit(1);
		}
		else if (property_name.compareToIgnoreCase("SpontaneousPinguCreation") == 0 || property_name.compareToIgnoreCase("t1p1") == 0)
		{
			// P1
			//filename = "simple-id-ok.xml";
			//filename = "/home/sylvain/Workspaces/pingu-generator/Source/Generator/rebel-walker-" + num_pingus + ".xml";
			prop = new SpontaneousPinguCreation();
			runAndCollectPingus(prop, filename);
		}
		else if (property_name.compareToIgnoreCase("EndlessBashing") == 0 || property_name.compareToIgnoreCase("t1p2") == 0)
		{
			// P2
			//filename = "basher-blocker-nok.xml";
			prop = new EndlessBashing();
			runAndCollectPingus(prop, filename);
		}
		else if (property_name.compareToIgnoreCase("TurnAround") == 0 || property_name.compareToIgnoreCase("t1p3") == 0)
		{
			// P3
			//filename = "simple-collides-nok.xml";
			//filename = "/home/sylvain/Workspaces/pingu-generator/Source/Generator/rebel-walker-" + num_pingus + ".xml";
			prop = new TurnAround();
			runAndCollectPingus(prop, filename);
		}
		else if (property_name.compareToIgnoreCase("AuctionBidding") == 0 || property_name.compareToIgnoreCase("t2p1") == 0)
		{
			//filename = "/home/sylvain/Workspaces/crv2016/Source/Pingus/auction_test";
			prop = new Auction();
			runAndCollectMarqPush(prop, filename);
			//System.out.println("Active slices: " + ((Auction) prop).slicer.getActiveSliceCount());
			//System.out.println("Closed slices: " + ((Auction) prop).slicer.getClosedSliceCount());
		}
		else if (property_name.compareToIgnoreCase("CandidateSelection") == 0 || property_name.compareToIgnoreCase("t2p2") == 0)
		{
			//filename = "/home/sylvain/Workspaces/crv2016/Source/Pingus/candidates_test";
			//filename = "/home/sylvain/Workspaces/crv2016/Source/Pingus/cand_sel_invalid1";
			runAndCollectMarq(new CandidateSelection(), filename);
		}
		else if (property_name.compareToIgnoreCase("SQLInjection") == 0 || property_name.compareToIgnoreCase("t2p3") == 0)
		{
			//filename = "/home/sylvain/Workspaces/crv2016/Source/Pingus/dummy_sanitize";
			runAndCollectMarqPush(new Sanitization(), filename);
		}
		else
		{
			System.err.println("BeepBeep won't run on this benchmark\nStatus: GaveUp");
			System.exit(0);
		}
		System.exit(0);
	}

	public static void runAndCollectPingus(Processor property, String filename) throws ConnectorException
	{
		LineReader s_reader = new LineReader(new File(filename));
		XmlReader x_reader = new XmlReader("<message>", "</message>");
		TokenFeeder.s_printStatus = true; // Print status line
		Connector.connect(s_reader, x_reader, property);
		Pullable p = property.getPullableOutput(0);
		long beg = System.currentTimeMillis();
		Object o = null;
		int pull_count = 0;
		while (p.hasNextHard() != NextStatus.NO)
		{
			pull_count++;
			o = p.pullHard();
			if (o != Troolean.Value.INCONCLUSIVE)
			{
				break;
			}
		}
		long end = System.currentTimeMillis();
		System.out.println(printStatus(o));
		System.out.println("Duration: " + (end - beg) / 1000 + " s, pulls: " + pull_count);
	}

	public static void runAndCollectMarq(Processor property, String filename) throws ConnectorException
	{
		LineReader reader = new LineReader(new File(filename));
		LineReader.s_printStatus = true; // Print status line
		FunctionProcessor feeder = new FunctionProcessor(CsvToList.instance);
		Connector.connect(reader, feeder, property);
		Pullable p = property.getPullableOutput(0);
		long beg = System.currentTimeMillis();
		Object o = null;
		int pull_count = 0;
		while (p.hasNextHard() != NextStatus.NO)
		{
			pull_count++;
			o = p.pullHard();
		}
		long end = System.currentTimeMillis();
		System.out.println(printStatus(o));
		System.out.println("Duration: " + (end - beg) / 1000 + " s, pulls: " + pull_count);
	}
	
	public static void runAndCollectMarqPush(Processor property, String filename) throws ConnectorException
	{
		FunctionProcessor feeder = new FunctionProcessor(CsvToList.instance);
		Connector.connect(feeder, property);
		long beg = System.currentTimeMillis();
		Object o = null;
		int pull_count = 0;
		Scanner scan;
		try {
			scan = new Scanner(new File(filename));
		} catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			System.err.println("ERROR: file not found");
			return;
		}
		SinkLast qsl = new SinkLast(1);
		Connector.connect(property, qsl);
		int line_count = 0;
		Pushable push = feeder.getPushableInput(0);
		while (scan.hasNextLine())
		{
			line_count++;
			if (line_count % 10000 == 0)
			{
				System.out.print("Lines read: " + line_count + "    \r");
				/*if (line_count % 10000 == 0 && property instanceof Auction)
				{
					System.out.println("Active slices: " + ((Auction) property).slicer.getActiveSliceCount());
					System.out.println("Closed slices: " + ((Auction) property).slicer.getClosedSliceCount());
				}*/
			}
			String line = scan.nextLine() + "\n";
			push.push(line);
		}
		long end = System.currentTimeMillis();
		System.out.println(printStatus(qsl.getLast()[0]));
		System.out.println("Duration: " + (end - beg) / 1000 + " s, pulls: " + pull_count);
		scan.close();
	}


	public static String printStatus(Object o)
	{
		StringBuilder out = new StringBuilder();
		out.append("\nStatus: ");
		if (o == null)
		{
			out.append("GaveUp");
		}
		else if (o == Troolean.Value.TRUE || o == Troolean.Value.INCONCLUSIVE)
		{
			out.append("Satisfied");
		}
		else if (o == Troolean.Value.FALSE)
		{
			out.append("Violated");
		}
		else
		{
			out.append("GaveUp");
		}
		return out.toString();
	}
}
