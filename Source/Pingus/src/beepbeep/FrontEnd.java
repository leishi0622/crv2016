/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beepbeep;

import java.io.File;
import java.io.FileNotFoundException;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Processor;
import ca.uqac.lif.cep.Pullable;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.Pullable.NextStatus;
import ca.uqac.lif.cep.io.LineReader;
import ca.uqac.lif.cep.ltl.Troolean;
import ca.uqac.lif.cep.xml.XmlReader;

/**
 * Front-end for running the MarQ benchmarks with BeepBeep 3
 */
public class FrontEnd 
{
	public static void main(String[] args) throws FileNotFoundException, ConnectorException
	{
		String num_pingus = "050";
		String filename = "/home/sylvain/Workspaces/pingu-generator/Source/Generator/all-ok-" + num_pingus + ".xml";
		Processor prop = null;
		{
			filename = "simple-id-nok.xml";
			//filename = "/home/sylvain/Workspaces/pingu-generator/Source/Generator/rebel-walker-" + num_pingus + ".xml";
			prop = new SpontaneousPinguCreation();
		}
		{
			//filename = "simple-collides-ok2.xml";
			//filename = "/home/sylvain/Workspaces/pingu-generator/Source/Generator/rebel-walker-" + num_pingus + ".xml";
			//prop = new TurnAround();
		}
		{
			//filename = "basher-blocker-nok.xml";
			//prop = new EndlessBashing();
		}
		runAndCollect(prop, filename);
	}
	
	public static void runAndCollect(Processor property, String filename) throws ConnectorException
	{
		LineReader s_reader = new LineReader(new File(filename));
		XmlReader x_reader = new XmlReader("<message>", "</message>");
		Connector.connect(s_reader, x_reader, property);
		Pullable p = property.getPullableOutput(0);
		long beg = System.currentTimeMillis();
		Object o = null;
		int pull_count = 0;
		while (p.hasNextHard() != NextStatus.NO)
		{
			pull_count++;
			o = p.pullHard();
			if (o != Troolean.Value.INCONCLUSIVE)
			{
				break;
			}
		}
		long end = System.currentTimeMillis();
		System.out.println(o);
		System.out.println("Duration: " + (end - beg) / 1000 + " s, pulls: " + pull_count);
	}
}
