/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beepbeep;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.Context;
import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.SmartFork;
import ca.uqac.lif.cep.epl.Trim;
import ca.uqac.lif.cep.functions.ArgumentPlaceholder;
import ca.uqac.lif.cep.functions.ConstantFunction;
import ca.uqac.lif.cep.functions.Equals;
import ca.uqac.lif.cep.functions.FunctionProcessor;
import ca.uqac.lif.cep.functions.FunctionTree;
import ca.uqac.lif.cep.ltl.Always;
import ca.uqac.lif.cep.ltl.ForAll;
import ca.uqac.lif.cep.ltl.TrooleanImplies;
import ca.uqac.lif.cep.ltl.Troolean;
import ca.uqac.lif.cep.ltl.TrooleanCast;
import ca.uqac.lif.cep.numbers.AbsoluteValue;
import ca.uqac.lif.cep.numbers.IsLessThan;
import ca.uqac.lif.cep.numbers.Signum;
import ca.uqac.lif.cep.numbers.Subtraction;
import ca.uqac.lif.cep.xml.XPathFunction;
import ca.uqac.lif.cep.xml.XPathFunctionGetNumber;

public class TurnAround extends GroupProcessor
{
	public TurnAround() throws ConnectorException
	{
		super(1, 1);
		GroupProcessor next_g = new GroupProcessor(1, 1);
		{
			SmartFork fork = new SmartFork(2);
			next_g.addProcessor(fork);
			next_g.associateInput(0, fork, 0);
			FunctionProcessor distance_x_now = new FunctionProcessor(new DistanceX());
			next_g.addProcessor(distance_x_now);
			Connector.connect(fork, distance_x_now, 0, 0);
			Trim next = new Trim(3);
			next_g.addProcessor(next);
			Connector.connect(fork, next, 1, 0);
			FunctionProcessor distance_x_later = new FunctionProcessor(new DistanceX());
			next_g.addProcessor(distance_x_later);
			Connector.connect(next, distance_x_later);
			FunctionProcessor gt = new FunctionProcessor(IsLessThan.instance);
			next_g.addProcessor(gt);
			Connector.connect(distance_x_now, gt, 0, 0);
			Connector.connect(distance_x_later, gt, 0, 1);
			FunctionProcessor cast = new FunctionProcessor(TrooleanCast.instance);
			next_g.addProcessor(cast);
			Connector.connect(gt, cast);
			next_g.associateOutput(0, cast, 0);
		}
		GroupProcessor imp_g = new GroupProcessor(1, 1);
		SmartFork fork = new SmartFork(2);
		imp_g.addProcessor(fork);
		FunctionProcessor collides = new FunctionProcessor(new Collides());
		imp_g.addProcessor(collides);
		Connector.connect(fork, collides, 0, 0);
		//imp_g.addProcessor(fa3);
		imp_g.addProcessor(next_g);
		Connector.connect(fork, next_g, 1, 0);
		TrooleanImplies implies = new TrooleanImplies();
		imp_g.addProcessor(implies);
		Connector.connect(collides, implies, 0, 0);
		Connector.connect(next_g, implies, 0, 1);
		imp_g.associateInput(0, fork, 0);
		imp_g.associateOutput(0, implies, 0);
		ForAll fa2 = new ForAll("p2", new XPathFunction("message/characters/character[status=BLOCKER]/id/text()"), imp_g);
		ForAll fa1 = new ForAll("p1", new XPathFunction("message/characters/character[status=WALKER]/id/text()"), fa2);
		Always a = new Always();
		Connector.connect(fa1, a);
		associateInput(0, fa1, 0);
		associateOutput(0, a, 0);
	}

	/**
	 * Predicate v &ne; v'
	 */
	protected static class NotSameSign extends FunctionTree
	{
		public NotSameSign()
		{
			super(Troolean.NOT_FUNCTION);
			FunctionTree cast = new FunctionTree(TrooleanCast.instance);
			FunctionTree sig_left = new FunctionTree(Signum.instance, new ArgumentPlaceholder("v"));
			FunctionTree sig_right = new FunctionTree(Signum.instance, new XPathFunctionGetNumber("message/characters/character[id=$p1]/velocity/x/text()"));
			FunctionTree equals = new FunctionTree(Equals.instance, sig_left, sig_right);
			cast.setChild(0, equals);
			setChild(0, cast);
		}
	}
	
	/**
	 * Function |x<sub>1</sub>-x<sub>2</sub>|
	 */
	protected static class DistanceX extends FunctionTree
	{
		public DistanceX()
		{
			super(AbsoluteValue.instance);
			FunctionTree minus = new FunctionTree(Subtraction.instance);
			minus.setChild(0, new XPathFunctionGetNumber("message/characters/character[id=$p1]/position/x/text()"));
			minus.setChild(1, new XPathFunctionGetNumber("message/characters/character[id=$p2]/position/x/text()"));
			setChild(0, minus);
		}
		
		public DistanceX clone()
		{
			DistanceX dx = new DistanceX();
			return dx;
		}
		
		public DistanceX clone(Context context)
		{
			DistanceX dx = new DistanceX();
			dx.setContext(context);
			return dx;
		}
		
		public Object[] evaluate(Object[] inputs, Context context)
		{
			Object[] out = super.evaluate(inputs, context);
			return out;
		}
	}

	/**
	 * Predicate |x<sub>1</sub>-x<sub>2</sub>| &lt; 6 &and; |y<sub>1</sub>-y<sub>2</sub>| < 10 
	 */
	protected static class Collides extends FunctionTree
	{
		public static final int X_RADIUS = 6;
		public static final int Y_RADIUS = 10;
		
		public Collides()
		{
			super(Troolean.AND_FUNCTION);
			{
				// Build the expression |p1//x - p2//x| < 6
				FunctionTree cast = new FunctionTree(TrooleanCast.instance);
				FunctionTree lt = new FunctionTree(IsLessThan.instance);
				FunctionTree abs = new FunctionTree(AbsoluteValue.instance);
				FunctionTree minus = new FunctionTree(Subtraction.instance);
				minus.setChild(0, new XPathFunctionGetNumber("message/characters/character[id=$p1]/position/x/text()"));
				minus.setChild(1, new XPathFunctionGetNumber("message/characters/character[id=$p2]/position/x/text()"));
				abs.setChild(0, minus);
				lt.setChild(0, abs);
				lt.setChild(1, new ConstantFunction(X_RADIUS));
				cast.setChild(0, lt);
				setChild(0, cast);
			}
			{
				// Build the expression |p1//y - p2//y| < 10
				FunctionTree cast = new FunctionTree(TrooleanCast.instance);
				FunctionTree lt = new FunctionTree(IsLessThan.instance);
				FunctionTree abs = new FunctionTree(AbsoluteValue.instance);
				FunctionTree minus = new FunctionTree(Subtraction.instance);
				minus.setChild(0, new XPathFunctionGetNumber("message/characters/character[id=$p1]/position/y/text()"));
				minus.setChild(1, new XPathFunctionGetNumber("message/characters/character[id=$p2]/position/y/text()"));
				abs.setChild(0, minus);
				lt.setChild(0, abs);
				lt.setChild(1, new ConstantFunction(Y_RADIUS));
				cast.setChild(0, lt);
				setChild(1, cast);
			}
		}
		
		public FunctionTree clone()
		{
			return new Collides();
		}
		
		public FunctionTree clone(Context context)
		{
			Collides col = new Collides();
			col.setContext(context);
			return col;
		}
	}
}
