/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marq;

import ca.uqac.lif.cep.Connector;
import ca.uqac.lif.cep.Connector.ConnectorException;
import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.epl.Last;
import ca.uqac.lif.cep.fol.ExistsFunction;
import ca.uqac.lif.cep.fol.ForAllFunction;
import ca.uqac.lif.cep.fol.LazyBooleanFunction;
import ca.uqac.lif.cep.fol.Predicate;
import ca.uqac.lif.cep.fol.InterpretationBuilder;
import ca.uqac.lif.cep.fol.PredicateAssertion;
import ca.uqac.lif.cep.fol.PredicateTupleReader;
import ca.uqac.lif.cep.fol.Predicate.Wildcard;
import ca.uqac.lif.cep.functions.ArgumentPlaceholder;
import ca.uqac.lif.cep.functions.FunctionProcessor;
import ca.uqac.lif.cep.ltl.TrooleanCast;

public class CandidateSelection extends GroupProcessor
{
	public CandidateSelection() throws ConnectorException
	{
		super(1, 1);
		PredicateTupleReader reader = new PredicateTupleReader();
		InterpretationBuilder builder = new InterpretationBuilder();
		builder.add(
				new Predicate("member", "V", "P"), 
				new Predicate("candidate", "C", "P"), 
				new Predicate("rank", "V", "C", "N"));
		Last last = new Last();
		ForAllFunction expression = new ForAllFunction("v", "V",
				new ExistsFunction("p", "P",
						new LazyBooleanFunction.And(
								new PredicateAssertion("member", new ArgumentPlaceholder("v"), new ArgumentPlaceholder("p")),
								new ForAllFunction("c", "C", 
										new LazyBooleanFunction.Implies(
												new PredicateAssertion("candidate", new ArgumentPlaceholder("c"), new ArgumentPlaceholder("p")),
												new PredicateAssertion("rank", new ArgumentPlaceholder("v"), new ArgumentPlaceholder("c"), Wildcard.instance)))
								)));
		FunctionProcessor forall = new FunctionProcessor(expression);
		FunctionProcessor cast = new FunctionProcessor(TrooleanCast.instance);
		Connector.connect(reader, builder, last, forall, cast);
		addProcessors(reader, builder, last, forall, cast);
		associateInput(0, reader, 0);
		associateOutput(0, cast, 0);
	}
	
	/*
		super(1, 1);
		PredicateTupleReader reader = new PredicateTupleReader();
		InterpretationBuilder builder = new InterpretationBuilder();
		builder.add(
				new Predicate("member", "V", "P"), 
				new Predicate("candidate", "C", "P"), 
				new Predicate("rank", "V", "C", "N"));
		Connector.connect(reader, builder);
		Last last = new Last();
		Connector.connect(builder, last);
		ForAll fa_c = new ForAll("c", new GetDomain("C"), new FunctionProcessor(
				new FunctionTree(BooleanFunction.AND_FUNCTION,
						new PredicateAssertion("member", new ArgumentPlaceholder("v"), new ArgumentPlaceholder("p")),
						new FunctionTree(BooleanFunction.IMPLIES_FUNCTION,
							new PredicateAssertion("candidate", new ArgumentPlaceholder("c"), new ArgumentPlaceholder("p")),
							new PredicateAssertion("rank", new ArgumentPlaceholder("v"), new ArgumentPlaceholder("c"), Wildcard.instance)
				))));
		Exists ex_p = new Exists("p", new GetDomain("P"), fa_c);
		ForAll fa_v = new ForAll("v", new GetDomain("V"), ex_p);
		Connector.connect(last, fa_v);
		addProcessors(fa_v, reader, builder, last);
		associateInput(0, reader, 0);
		associateOutput(0, fa_v, 0);	 
	 */
}
