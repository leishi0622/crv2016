/*
    BeepBeep processor chains for the CRV 2016
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marq;

import ca.uqac.lif.cep.GroupProcessor;
import ca.uqac.lif.cep.fsm.FunctionTransition;
import ca.uqac.lif.cep.fsm.MooreMachine;
import ca.uqac.lif.cep.fsm.MooreMachine.TransitionOtherwise;
import ca.uqac.lif.cep.functions.ArgumentPlaceholder;
import ca.uqac.lif.cep.functions.ConstantFunction;
import ca.uqac.lif.cep.functions.ContextAssignment;
import ca.uqac.lif.cep.functions.Equals;
import ca.uqac.lif.cep.functions.FunctionTree;
import ca.uqac.lif.cep.functions.Negation;
import ca.uqac.lif.cep.functions.TracePlaceholder;
import ca.uqac.lif.cep.ltl.BooleanFunction;
import ca.uqac.lif.cep.ltl.Troolean;
import ca.uqac.lif.cep.sets.Contains;
import ca.uqac.lif.cep.sets.Multiset;
import ca.uqac.lif.cep.sets.MultisetAddElement;
import ca.uqac.lif.cep.sets.NthElement;

public class Sanitization extends GroupProcessor 
{
	public Sanitization()
	{
		super(1, 1);
		MooreMachine m = new MooreMachine(1, 1);
		m.setContext("S", new Multiset());
		m.addTransition(0, new FunctionTransition(
				new FunctionTree(Equals.instance,
						new FunctionTree(new NthElement(0), new TracePlaceholder(0)),
						new ConstantFunction("sanitise")), 0,
						new ContextAssignment("S", new FunctionTree(MultisetAddElement.instance,
								new ArgumentPlaceholder("S"),
								new FunctionTree(new NthElement(1), new TracePlaceholder(0))))));
		m.addTransition(0, new FunctionTransition(
				new FunctionTree(BooleanFunction.AND_FUNCTION,
						new FunctionTree(Equals.instance,
								new FunctionTree(new NthElement(0), new TracePlaceholder(0)),
								new ConstantFunction("derive")),
								new FunctionTree(Contains.instance,
										new ArgumentPlaceholder("S"),
										new FunctionTree(new NthElement(1), new TracePlaceholder(0)))), 0,
						new ContextAssignment("S", new FunctionTree(MultisetAddElement.instance,
								new ArgumentPlaceholder("S"),
									new FunctionTree(new NthElement(2), new TracePlaceholder(0))))));
		m.addTransition(0, new FunctionTransition(
				new FunctionTree(BooleanFunction.AND_FUNCTION,
						new FunctionTree(Equals.instance,
								new FunctionTree(new NthElement(0), new TracePlaceholder(0)),
								new ConstantFunction("use")),
								new FunctionTree(Negation.instance,
										new FunctionTree(Contains.instance,
												new ArgumentPlaceholder("S"),
												new FunctionTree(new NthElement(1), new TracePlaceholder(0))))), 1));
		m.addTransition(0, new TransitionOtherwise(0));
		m.addTransition(1, new TransitionOtherwise(1));
		m.addSymbol(0, Troolean.Value.TRUE);
		m.addSymbol(1, Troolean.Value.FALSE);
		associateInput(0, m, 0);
		associateOutput(0, m, 0);
	}
}
